<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DataTamu extends Model
{
    use HasFactory;

    protected $table = 'data_tamus';
    protected $fillable = ['nama', 'alamat', 'no_hp', 'tgl'];

    public function post()
    {
        return $this->hasMany(Post::class);
    }
}
