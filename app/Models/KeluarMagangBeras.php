<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class KeluarMagangBeras extends Model
{
    use HasFactory;

    protected $table = 'keluar_magang_beras';
    protected $fillable = ['nama', 'alamat', 'no_hp', 'tgl', 'nominal'];

    public function post()
    {
        return $this->hasMany(Post::class);
    }
}
