<?php

namespace App\Http\Controllers;

use App\Models\DataTamu;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DataTamu2Controller extends Controller
{
    public function index()
    {
        $data_tamu = DataTamu::all();

        return view('Anggota/home', compact('data_tamu'));
    }

    public function edit($id)
    {
        $datatamu = DataTamu::findOrFail($id);
        return view('Anggota/edittamu')->with([
            "datatamu" => $datatamu,
        ]);
    }

    public function update(Request $request, $id)
    {
        $datatamu = DataTamu::findOrFail($id);
        $datatamu->update($request->all());
        // $request->validate([
        //     'nama' => 'required',
        //     'asal_instansi' => 'required',
        //     'tujuan' => 'required',
        //     'no_hp' => 'required',
        //     'tgl' => 'required',
        // ]);

        // daftartamu::update($request->all());


        return redirect('Anggota/home');
    }


    public function destroy($id)

    {
        $datatamu = DataTamu::findOrFail($id);
        $datatamu->delete();
        return redirect('Anggota/home');
    }
}
