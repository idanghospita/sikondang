<?php

namespace App\Http\Controllers;

use App\Models\KeluarMagangBeras;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ReadMagangBerasController extends Controller
{
    public function index()
    {
        $keluar_magang_beras = KeluarMagangBeras::all();

        return view('Anggota/k_beras', compact('keluar_magang_beras'));
    }
}
