<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\KeluarMagangPadi;

class MagangKeluarPadiController extends Controller
{
    public function index()
    {


        return view('/tkeluarpadi');
    }
    public function create()
    {
        return view('/tkeluarpadi');
    }

    public function store(Request $request)
    {

        $request->validate([
            'nama' => 'required',
            'alamat' => 'required',
            'no_hp' => 'required',
            'tgl' => 'required',
            'nominal' => 'required',
        ]);

        KeluarMagangPadi::create($request->all());
        $request->session()->flash('sukses', '
        <div class="alert alert-success" role="alert">
            Data berhasil ditambahkan
        </div>
    ');

        return redirect('k_padi');
    }

    public function edit($id)
    {
        $keluar_magang_padi = KeluarMagangPadi::findOrFail($id);
        return view('editmagangpadi')->with([
            "keluar_magang_padi" => $keluar_magang_padi,
        ]);
    }

    public function update(Request $request, $id)
    {
        $keluar_magang_padi = KeluarMagangPadi::findOrFail($id);
        $keluar_magang_padi->update($request->all());
        // $request->validate([
        //     'nama' => 'required',
        //     'asal_instansi' => 'required',
        //     'tujuan' => 'required',
        //     'no_hp' => 'required',
        //     'tgl' => 'required',
        // ]);

        // daftartamu::update($request->all());


        return redirect('k_padi');
    }


    public function destroy($id)

    {
        $keluar_magang_padi = KeluarMagangPadi::findOrFail($id);
        $keluar_magang_padi->delete();
        return redirect('k_padi');
    }
}
