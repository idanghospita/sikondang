<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\KeluarMagangBeras;

class MagangKeluarBerasController extends Controller
{
    public function index()
    {
        // $keluar_magang_beras = KeluarMagangBeras::all();

        // return view('Anggota/k_beras', compact('keluar_magang_beras'));
        return view('/tkeluarberas');
    }
    public function create()
    {
        return view('/tkeluarberas');
    }

    public function store(Request $request)
    {

        $request->validate([
            'nama' => 'required',
            'alamat' => 'required',
            'no_hp' => 'required',
            'tgl' => 'required',
            'nominal' => 'required',
        ]);

        KeluarMagangBeras::create($request->all());
        $request->session()->flash('sukses', '
        <div class="alert alert-success" role="alert">
            Data berhasil ditambahkan
        </div>
    ');

        return redirect('k_beras');
    }

    public function edit($id)
    {
        $keluar_magang_beras = KeluarMagangBeras::findOrFail($id);
        return view('editmagangberas')->with([
            "keluar_magang_beras" => $keluar_magang_beras,
        ]);
    }

    public function update(Request $request, $id)
    {
        $keluar_magang_beras = KeluarMagangBeras::findOrFail($id);
        $keluar_magang_beras->update($request->all());
        // $request->validate([
        //     'nama' => 'required',
        //     'asal_instansi' => 'required',
        //     'tujuan' => 'required',
        //     'no_hp' => 'required',
        //     'tgl' => 'required',
        // ]);

        // daftartamu::update($request->all());


        return redirect('k_beras');
    }


    public function destroy($id)

    {
        $keluar_magang_beras = KeluarMagangBeras::findOrFail($id);
        $keluar_magang_beras->delete();
        return redirect('k_beras');
    }
}
