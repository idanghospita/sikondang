<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\KeluarMagangUang;

class MagangKeluarUangController extends Controller
{
    public function index()
    {


        return view('/tkeluaruang');
    }
    public function create()
    {
        return view('/tkeluaruang');
    }

    public function store(Request $request)
    {

        $request->validate([
            'nama' => 'required',
            'alamat' => 'required',
            'no_hp' => 'required',
            'tgl' => 'required',
            'nominal' => 'required',
        ]);

        KeluarMagangUang::create($request->all());
        $request->session()->flash('sukses', '
        <div class="alert alert-success" role="alert">
            Data berhasil ditambahkan
        </div>
    ');

        return redirect('k_uang');
    }

    public function edit($id)
    {
        $keluar_magang_uang = KeluarMagangUang::findOrFail($id);
        return view('editmaganguang')->with([
            "keluar_magang_uang" => $keluar_magang_uang,
        ]);
    }

    public function update(Request $request, $id)
    {
        $keluar_magang_uang = KeluarMagangUang::findOrFail($id);
        $keluar_magang_uang->update($request->all());
        // $request->validate([
        //     'nama' => 'required',
        //     'asal_instansi' => 'required',
        //     'tujuan' => 'required',
        //     'no_hp' => 'required',
        //     'tgl' => 'required',
        // ]);

        // daftartamu::update($request->all());


        return redirect('k_uang');
    }


    public function destroy($id)

    {
        $keluar_magang_uang = KeluarMagangUang::findOrFail($id);
        $keluar_magang_uang->delete();
        return redirect('k_uang');
    }
}
