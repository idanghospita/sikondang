<?php

namespace App\Http\Controllers;

use App\Models\KeluarMagangUang;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ReadMagangUangController extends Controller
{
    public function index()
    {
        $keluar_magang_uang = KeluarMagangUang::all();

        return view('Anggota/k_uang', compact('keluar_magang_uang'));
    }
}
