<?php

namespace App\Http\Controllers;

use App\Models\KeluarMagangPadi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ReadMagangPadiController extends Controller
{
    public function index()
    {
        $keluar_magang_padi = KeluarMagangPadi::all();

        return view('Anggota/k_padi', compact('keluar_magang_padi'));
    }
}
