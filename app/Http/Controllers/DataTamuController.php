<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\DataTamu;

class DataTamuController extends Controller
{
    public function index()
    {
        return view('/tambahtamu');
    }
    public function create()
    {
        return view('/tambahtamu');
    }

    public function store(Request $request)
    {

        $request->validate([
            'nama' => 'required',
            'alamat' => 'required',
            'no_hp' => 'required',
            'tgl' => 'required',
        ]);

        DataTamu::create($request->all());
        $request->session()->flash('sukses', '
        <div class="alert alert-success" role="alert">
            Data berhasil ditambahkan
        </div>
    ');

        return redirect('Anggota/home');
    }
}
