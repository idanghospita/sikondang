<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('landing');
});
Route::get('admin', function () {
    return view('admin2/admin-home');
});
Route::get('admin-hajatan', function () {
    return view('admin2/admin-hajatan');
});
Route::get('admin-chat', function () {
    return view('admin2/admin-chat');
});
// Route::get('/coba', function () {
//     return view('coba');
// });
// Route::get('/home', function () {
//     return view('home');
// });
// Route::get('/login', function () {
//     return view('login');
// });
Route::get('/register', function () {
    return view('register');
});
Route::get('/tmagangpadi', function () {
    return view('tmagangpadi');
});
Route::get('/tmaganguang', function () {
    return view('tmaganguang');
});
// Route::get('/tmagangberas', function () {
//     return view('tmagangberas');
// });
Route::get('/thutangberas', function () {
    return view('tmaganguang');
});
Route::get('/thutangpadi', function () {
    return view('thutangpadi');
});
Route::get('/thutanguang', function () {
    return view('thutanguang');
});
// Route::get('/tkeluarberas', function () {
//     return view('tkeluarberas');
// });
// Route::get('/tkeluarpadi', function () {
//     return view('tkeluarpadi');
// });
Route::get('/tkeluaruang', function () {
    return view('tkeluaruang');
});
// Route::resource('input', DataTamuController::class);

Route::get('/magangberas', function () {
    return view('Anggota/m_beras');
});
Route::get('/magangpadi', function () {
    return view('Anggota/m_padi');
});
Route::get('/maganguang', function () {
    return view('Anggota/m_uang');
});
Route::get('/hutangberas', function () {
    return view('Anggota/h_beras');
});
Route::get('/hutangpadi', function () {
    return view('Anggota/h_padi');
});
Route::get('/hutanguang', function () {
    return view('Anggota/h_uang');
});
// Route::get('/keluarberas', function () {
//     return view('Anggota/k_beras');
// });
// Route::get('/keluarpadi', function () {
//     return view('Anggota/k_padi');
// });
// Route::get('/k_uang', function () {
//     return view('Anggota/k_uang');
// });
Route::get('/chat', function () {
    return view('Anggota/chat');
});



Route::resource('tambahtamu', 'DataTamuController');
Route::resource('home', 'DataTamu2Controller');
// Route::resource('edit', 'DataTamu2Controller');
Route::resource('edittamu', 'DataTamu2Controller');
Route::resource('tkeluarberas', 'MagangKeluarBerasController');
Route::resource('tkeluarpadi', 'MagangKeluarPadiController');
Route::resource('tkeluaruang', 'MagangKeluarUangController');
Route::resource('k_beras', 'ReadMagangBerasController');
Route::resource('k_padi', 'ReadMagangPadiController');
Route::resource('k_uang', 'ReadMagangUangController');
Route::resource('editmagangberas', 'MagangKeluarBerasController');
Route::resource('editmagangpadi', 'MagangKeluarPadiController');
Route::resource('editmaganguang', 'MagangKeluarUangController');
Route::resource('tmagangberas', 'TambahMagangBerasController');
Route::resource('tmagangpadi', 'TambahMagangPadiController');
Route::resource('tmaganguang', 'TambahMagangUangController');
Route::resource('thutangberas', 'TambahHutangBerasController');
Route::resource('thutangpadi', 'TambahHutangPadiController');
Route::resource('thutanguang', 'TambahHutangUangController');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
