@extends('layouts.main')
@section('content')
<div class="main-panel">
    <div class="content-wrapper">
      <div class="row">
        <div class="col-lg-6 grid-margin stretch-card">
          <div class="card">
          </div>
        </div>
        <div class="col-lg-12 grid-margin stretch-card">
          <div class="card" >
            <div class="card-body" style="overflow-x:auto;">
             
              <h4 class="card-title">DATA PEMASUKAN MAGANG UANG</h4>
              <a href="tmaganguang" class="btn btn-primary btn-sm"><i class="fas fa-plus"></i> Tambah Magang Uang</a>
              <a href="/tambahtamu" class="btn btn-primary btn-sm"><i class="fas fa-plus"></i> Cetak PDF</a>
              <table id="example" class="table table-striped table-bordered nowrap " style="width:100%">
                <thead>
                    <tr>
                        <th>NO</th>
                        <th>NAMA</th>
                        <th>ALAMAT</th>
                        <th>NO HP</th>
                        <th>NOMINAL</th>
                        <th>TANGGAL</th>
                        <th>AKSI</th>
                    </tr>
                </thead>
                <tbody>
                   
                   
                </tbody>
            </table>
            </div>
          </div>
        </div>
        <div class="col-lg-12 grid-margin stretch-card">
          <div class="card">
           
          </div>
        </div>
        <div class="col-lg-12 stretch-card">
        
        </div>
      </div>
    </div>
    <!-- content-wrapper ends -->
    <!-- partial:../../partials/_footer.html -->
    <footer class="footer">
      <div class="d-sm-flex justify-content-center justify-content-sm-between">
                <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Copyright © 2021. All rights reserved.</span>
      </div>
    </footer>
    <!-- partial -->
  </div>
  @endsection

  