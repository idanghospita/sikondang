@extends('layouts.main')
@section('content')
<div class="main-panel">        
    <div class="content-wrapper">
      <div class="row">
        <div class="col-md-6 grid-margin stretch-card">
          <div class="card">
            <div class="card-body">
              <h4 class="card-title">Edit Magang Uang</h4>
              
                <form action="{{ route('editmaganguang.update', $keluar_magang_uang->id) }}"  method="POST">
                  @csrf
                  @method('PUT')
                <form class="forms-sample">
                  <div class="form-group">
                    <label for="nama">Nama</label>
                    <input type="string" class="form-control" id="nama" name="nama"  value="<?php echo $keluar_magang_uang['nama']; ?>"placeholder="Nama">
                  </div>
                  <div class="form-group">
                    <label for="alamat">Alamat</label>
                    <input type="string" class="form-control" id="alamat" name="alamat" value="<?php echo $keluar_magang_uang['alamat']; ?>"placeholder="Alamat">
                  </div>
                  <div class="form-group">
                    <label for="no_hp">NO HP</label>
                    <input type="char" class="form-control" id="no_hp" name="no_hp" value="<?php echo $keluar_magang_uang['no_hp']; ?>" placeholder="NO HP">
                  </div>
                  <div class="form-group">
                    <label for="tgl">Tanggal</label>
                    <input type="date" class="form-control" id="tgl" name="tgl" value="<?php echo $keluar_magang_uang['tgl']; ?>"placeholder="tgl">
                  </div>
                  <div class="form-group">
                    <label for="nominal">Nominal</label>
                    <input type="int" class="form-control" id="nominal" name="nominal" value="<?php echo $keluar_magang_uang['nominal']; ?>"placeholder="nominal">
                  </div>
                  <button type="submit" class="btn btn-primary me-2">Simpan</button>
                  <button class="btn btn-light">Cancel</button>
                </form>
            </div>
          </div>
        </div>
@endsection