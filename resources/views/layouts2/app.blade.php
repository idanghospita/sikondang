<!DOCTYPE html>
        <html lang="en">
        <head>
          <!-- Required meta tags -->
          <meta charset="utf-8">
          <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
          <title>SI-KONDANG </title>
          <!-- plugins:css -->
          <link rel="stylesheet" href="/assets/vendors/feather/feather.css">
          <link rel="stylesheet" href="/assets/vendors/feather/feather.css">
          <link rel="stylesheet" href="/assets/vendors/mdi/css/materialdesignicons.min.css">
          <link rel="stylesheet" href="/assets/vendors/ti-icons/css/themify-icons.css">
          <link rel="stylesheet" href="/assets/vendors/typicons/typicons.css">
          <link rel="stylesheet" href="/assets/vendors/simple-line-icons/css/simple-line-icons.css">
          <link rel="stylesheet" href="/assets/vendors/css/vendor.bundle.base.css">
          <link href="assets3/vendor/simple-datatables/style.css" rel="stylesheet">
         
          <!-- endinject -->
          <!-- Plugin css for this page -->
          <!-- End plugin css for this page -->
          <!-- inject:css -->
          <link rel="stylesheet" href="/assets/css/vertical-layout-light/style.css">
          <!-- endinject -->
          <link rel="shortcut icon" href="/images/favicon.png" /> 
        </head>
        
        <body>
            {{-- <div class="container-scroller">
                
                <!-- partial:../../partials/_navbar.html -->
             
                
                  <!-- partial -->
                <div class="container-fluid page-body-wrapper">
                  
                  <!-- partial:../../partials/_settings-panel.html -->
                  <div class="theme-setting-wrapper">
              </div>
              <div id="right-sidebar" class="settings-panel">
                <i class="settings-close ti-close"></i>
                
              </div>
             
              <!-- partial -->
              @yield('content')
            
              <!-- partial:../../partials/_sidebar.html -->
           
              <!-- partial -->
              
              <!-- main-panel ends -->
            </div> --}}
            <!-- page-body-wrapper ends -->
          </div>
          <!-- container-scroller -->
          <!-- plugins:js -->
          <script src="/assets/vendors/js/vendor.bundle.base.js"></script>
          <!-- endinject -->
          <!-- Plugin js for this page -->
          <script src="/assets/vendors/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
          <!-- End plugin js for this page -->
          <!-- inject:js -->
          <script src="/assets/js/off-canvas.js"></script>
          <script src="/assets/js/hoverable-collapse.js"></script>
          <script src="/assets/js/template.js"></script>
          <script src="/assets/js/settings.js"></script>
          <script src="/assets/js/todolist.js"></script>
          
        
          <!-- endinject -->
          <!-- Custom js for this page-->
          <!-- End custom js for this page-->
        </body>
        
        </html>
        

        <main class="py-4">
            @yield('content')
        </main>
    </div>
</body>
</html>
