<nav class="sidebar sidebar-offcanvas" id="sidebar">
    <ul class="nav">
      <li class="nav-item">
        <a class="nav-link" href="/home">
          <i class="mdi mdi-home menu-icon"></i>
          <span class="menu-title">Data Tamu</span>
        </a>
      </li>
      <li class="nav-item nav-category">KONDANGAN</li>
      <li class="nav-item">
        <a class="nav-link" data-bs-toggle="collapse" href="#form-elements" aria-expanded="false" aria-controls="form-elements">
          <i class="menu-icon mdi mdi-book-open-page-variant"></i>
          <span class="menu-title">Pemasukan Magang</span>
          <i class="menu-arrow"></i>
        </a>
        <div class="collapse" id="form-elements">
          <ul class="nav flex-column sub-menu">
            <li class="nav-item"><a class="nav-link" href="/magangberas">Beras</a></li>
            <li class="nav-item"><a class="nav-link" href="/magangpadi">Padi</a></li>
            <li class="nav-item"><a class="nav-link" href="/maganguang">Uang</a></li>
          </ul>
        </div>
      </li>
      <li class="nav-item">
        <a class="nav-link" data-bs-toggle="collapse" href="#charts" aria-expanded="false" aria-controls="charts">
          <i class="menu-icon mdi mdi-book-open-page-variant"></i>
          <span class="menu-title">Pemasukan Hutang</span>
          <i class="menu-arrow"></i>
        </a>
        <div class="collapse" id="charts">
          <ul class="nav flex-column sub-menu">
            <li class="nav-item"> <a class="nav-link" href="/hutangberas">Beras</a></li>
            <li class="nav-item"> <a class="nav-link" href="/hutangpadi">Padi</a></li>
            <li class="nav-item"> <a class="nav-link" href="/hutanguang">Uang</a></li>
          </ul>
        </div>
      </li>
      <li class="nav-item">
        <a class="nav-link" data-bs-toggle="collapse" href="#tables" aria-expanded="false" aria-controls="tables">
          <i class="menu-icon mdi mdi-book-open-page-variant"></i>
          <span class="menu-title">Pengeluaran Magang</span>
          <i class="menu-arrow"></i>
        </a>
        <div class="collapse" id="tables">
          <ul class="nav flex-column sub-menu">
            <li class="nav-item"> <a class="nav-link" href="/k_beras">Beras</a></li>
            <li class="nav-item"> <a class="nav-link" href="/k_padi">Padi</a></li>
            <li class="nav-item"> <a class="nav-link" href="/k_uang">Uang</a></li>
          </ul>
        </div>
      </li>

      <li class="nav-item">
        <a class="nav-link" href="/chat">
          <i class="mdi mdi-email menu-icon"></i>
          <span class="menu-title">Chat</span>
        </a>
      </li>
    </ul>
  </nav>