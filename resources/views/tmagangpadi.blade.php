@extends('layouts.main')
@section('content')
<div class="main-panel">        
    <div class="content-wrapper">
      <div class="row">
        <div class="col-md-6 grid-margin stretch-card">
          <div class="card">
            <div class="card-body">
              <h4 class="card-title">Tambah Magang Padi</h4>
              <form action=""  method="POST">
                @csrf
              <form class="forms-sample">
                <div class="form-group">
                    <label >Nama</label>
                    <select name ="data_tamu_id" class="form-control" >
                 
                      <option value=""> Pilih </option>
                      @foreach ($data_tamu as $tamu)
                      <option value="{{ $tamu->id }}"> {{ $tamu->nama}}</option>
                          
                      @endforeach
                    
                     
                    </select>
                  </div>
                <div class="form-group">
                  <label for="nama">Nominal</label>
                  <input type="string" class="form-control" id="nama" name="nama" placeholder="Kg">
                </div>

                <button type="submit" class="btn btn-primary me-2">Simpan</button>
                <button class="btn btn-light">Cancel</button>
              </form>
            </div>
          </div>
        </div>
@endsection