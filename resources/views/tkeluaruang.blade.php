@extends('layouts.main')
@section('content')
<div class="main-panel">        
    <div class="content-wrapper">
      <div class="row">
        <div class="col-md-6 grid-margin stretch-card">
          <div class="card">
            <div class="card-body">
              <h4 class="card-title">Tambah Magang Uang</h4>
              <form action="{{ route('tkeluaruang.store') }}"  method="POST">
                @csrf
              <form class="forms-sample">
                <div class="form-group">
                  <label for="nama">Nama</label>
                  <input type="string" class="form-control" id="nama" name="nama" placeholder="Nama">
                </div>
                <div class="form-group">
                  <label for="alamat">Alamat</label>
                  <input type="string" class="form-control" id="alamat" name="alamat"placeholder="Alamat">
                </div>
                <div class="form-group">
                  <label for="no_hp">NO HP</label>
                  <input type="char" class="form-control" id="no_hp" name="no_hp"placeholder="NO HP">
                </div>
                <div class="form-group">
                  <label for="tgl">Tanggal</label>
                  <input type="date" class="form-control" id="tgl" name="tgl"placeholder="tgl">
                </div>
                <div class="form-group">
                  <label for="tgl">Nominal</label>
                  <input type="int" class="form-control" id="nominal" name="nominal"placeholder="nominal">
                </div>
                <button type="submit" class="btn btn-primary me-2">Simpan</button>
                <button class="btn btn-light">Cancel</button>
              </form>
            </div>
          </div>
        </div>
@endsection